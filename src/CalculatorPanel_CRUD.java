import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CalculatorPanel_CRUD extends JPanel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField display;
    private JButton[] buttons;
    private JTextArea historyTextArea;
    private String dbPath;
    private JLabel loadingLabel;
    private JPopupMenu popupMenu;
    private JFrame frame;
    private JTable table;
    private boolean isPreviousButtonEqual;
    private javax.swing.table.DefaultTableModel tableModel;
    
    public CalculatorPanel_CRUD() {
        setLayout(new BorderLayout());
        
        // 创建显示结果的文本框
        display = new JTextField();
        display.setEditable(true);
        add(display, BorderLayout.NORTH);

        // 创建按钮面板，并设置布局为网格布局
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(5, 4));

        // 按钮的标签
        String[] buttonLabels = {
        		"history", "","C","←",
        		"7", "8", "9", "÷",
                "4", "5", "6", "×",
                "1", "2", "3", "-",
                ".", "0", "=", "+" 
        };

        // 初始化按钮数组
        buttons = new JButton[buttonLabels.length];
        
        // 创建按钮并添加到面板上
        for (int i = 0; i < buttonLabels.length; i++) {
            buttons[i] = new JButton(buttonLabels[i]);
	        if (buttonLabels[i].equals("history")) {
	            try {
	                // 读取图片文件并创建图标
	                ImageIcon icon = new ImageIcon(ImageIO.read(new File("res/history.png")));
	                buttons[i].setIcon(icon);
	                buttons[i].setText("");
	                buttons[i].setToolTipText("history");
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }else {
	            // 设置按钮字体大小
	            buttons[i].setFont(new Font("Arial", Font.PLAIN, (buttonLabels[i].equals("←"))?10:40));   	
	        }

            

            buttonPanel.add(buttons[i]);
        }

        // 添加按钮面板到主面板的中间
        add(buttonPanel, BorderLayout.CENTER);

        // 添加按钮事件监听器
        for (JButton button : buttons) {
            button.addActionListener(new ButtonClickListener());
        }
        //準備右側的歷史表單
        //JPanel historyPanel = new JPanel();
        // 创建历史文本区域并设置固定宽度
        historyTextArea = new JTextArea(3,5); // 设置行数和列数
        historyTextArea.setEditable(true); // 禁止编辑
        JScrollPane scrollPane = new JScrollPane(historyTextArea); // 添加滚动条
        
        
        add(scrollPane, BorderLayout.SOUTH);
        
        //=================表格產生=================
        JPanel main_tb = new JPanel();
        main_tb.setLayout(new BorderLayout());

        tableModel = new javax.swing.table.DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
            	return column != 0 && column != 1 && column != 3;
            }
        };
        table = new JTable(tableModel);
        javax.swing.table.TableRowSorter<javax.swing.table.DefaultTableModel> sorter = new javax.swing.table.TableRowSorter<>(tableModel);
        table.setRowSorter(sorter);
        // 添加表格列名
        tableModel.addColumn("ID");
        tableModel.addColumn("Command");
        tableModel.addColumn("Remark");
        tableModel.addColumn("DateTime");
        
        int[] columnWidths = {25, 100, 150, 120}; // 定义每一列的宽度
        for (int i = 0; i < columnWidths.length; i++) {
            table.getColumnModel().getColumn(i).setPreferredWidth(columnWidths[i]);
        }
        // 居中渲染器
        javax.swing.table.DefaultTableCellRenderer centerRenderer = new javax.swing.table.DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        
        // 添加单元格编辑器监听器
        table.getModel().addTableModelListener(e -> {
            if (e.getType() == TableModelEvent.UPDATE) {
                int row = e.getFirstRow();
                int column = e.getColumn();
                Object id = tableModel.getValueAt(row, 0);
                Object data = tableModel.getValueAt(row, column);
                String idString = id != null ? id.toString() : "";
                String dataString = data != null ? data.toString() : "";
                if (idString!="") {
                    String[] command_str = {idString, dataString};
                    DB_Update(command_str);
                }
                System.out.println("Cell updated: (" + row + ", " + column + ") - New value: " + data);
                // 在此处执行更新数据库的操作
            }
        });

        // 创建右键菜单
        popupMenu = new JPopupMenu();
        JMenuItem deleteMenuItem = new JMenuItem("Delete Selected Record");
        deleteMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	showDeleteConfirmationDialog();
            }
        });
        popupMenu.add(deleteMenuItem);

        // 将右键菜单添加到表格
        table.setComponentPopupMenu(popupMenu);
        // 添加鼠标监听器以在右键单击时选择行
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int[] selectedRows = table.getSelectedRows();
                if (SwingUtilities.isRightMouseButton(e) && selectedRows.length <= 1) {
                    // 获取鼠标点击的行索引
                    int row = table.rowAtPoint(e.getPoint());
                    if (row >= 0 && row < table.getRowCount()) {
                        // 选择行
                        table.setRowSelectionInterval(row, row);
                    }
                }
            }
        });
        
        // 添加按钮面板
        JPanel buttonPanel_tb = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton addButton = new JButton("Refresh");
        addButton.addActionListener(e -> DB_Retrieve());
        buttonPanel_tb.add(addButton);
        JButton removeButton = new JButton("RemoveAll");
        removeButton.addActionListener(e -> showDeleteAllConfirmationDialog());
        buttonPanel_tb.add(removeButton);

        // 加载动画
        ImageIcon loadingIcon = new ImageIcon("res/loading.gif");
        loadingLabel = new JLabel(loadingIcon);
        loadingLabel = new JLabel(new ImageIcon(loadingIcon.getImage().getScaledInstance(32, 32, Image.SCALE_DEFAULT)));
        loadingLabel.setBounds(65, 20, loadingIcon.getIconWidth(), loadingIcon.getIconHeight());
        loadingLabel.setVisible(false); // 默认隐藏

        // 创建一个 JLayeredPane

        JLayeredPane panel_tb = new JLayeredPane();
        panel_tb.setPreferredSize(new Dimension(400, 300));
        JScrollPane scrollPane_tb = new JScrollPane(table);
        scrollPane_tb.setBounds(0, 0, 400, 300); // 设置表格的位置和大小
        
        panel_tb.add(scrollPane_tb, JLayeredPane.DEFAULT_LAYER);
        panel_tb.add(loadingLabel, JLayeredPane.PALETTE_LAYER); // 将加载动画添加到窗口顶部   
        
        main_tb.add(panel_tb, BorderLayout.CENTER); // 将加载动画添加到窗口顶部
        main_tb.add(buttonPanel_tb, BorderLayout.NORTH);

        main_tb.setVisible(true);
        add(main_tb, BorderLayout.EAST);
    }
    
    private void showDeleteAllConfirmationDialog() {
        int dialogResult = JOptionPane.showConfirmDialog(frame,
                "Are you sure you want to delete all record?",
                "Confirm Delete",
                JOptionPane.YES_NO_OPTION);
        if (dialogResult == JOptionPane.YES_OPTION) {
            table_Delete(true);
        }
    }
    
    private void showDeleteConfirmationDialog() {
        int dialogResult = JOptionPane.showConfirmDialog(frame,
                "Are you sure you want to delete the selected rows?",
                "Confirm Delete",
                JOptionPane.YES_NO_OPTION);
        if (dialogResult == JOptionPane.YES_OPTION) {
            table_Delete(false);
        }
    }
    
    private void table_Delete(boolean isAll) {
    	if (isAll) {
            // 获取表格中的行数
            int rowCount = table.getRowCount();
            // 如果表格中有行，则选择第一行到最后一行
            if (rowCount > 0) {
                table.setRowSelectionInterval(0, rowCount - 1);
            }
    	}
        int[] selectedRows = table.getSelectedRows();
        if (selectedRows.length > 0) {
        	
            // 从表格中获取所选行的 ID
            ArrayList<String> ids = new ArrayList<>();
            for (int i : selectedRows) {
                String id = String.valueOf(table.getValueAt(i, 0));
                ids.add(id);
            }

            // 将 ID 连接成字符串
            String idList = String.join(", ", ids);

            // 在此执行删除操作
            String[] command_str = {idList};
            DB_Delete(command_str);
            
            for (int i = selectedRows.length - 1; i >= 0; i--) {
                tableModel.removeRow(selectedRows[i]);
            }
        }
    }
    
    private void DB_Create(String[] args) {
        // SQL 插入语句
        String sqlInsert = "INSERT INTO history (command_str) VALUES (?)";

        try (Connection connection = DriverManager.getConnection(dbPath);
             PreparedStatement statement = connection.prepareStatement(sqlInsert)) {
            // 设置插入参数
            statement.setString(1, args[0]);

            // 执行插入操作
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("数据插入成功！");
            } else {
                System.out.println("数据插入失败！");
            }
        } catch (SQLException e) {
            // 处理异常
            e.printStackTrace();
        }
    }

    private void DB_Retrieve() {	
    	tableModel.setRowCount(0);
    	loadingLabel.setVisible(true);
    	
        Timer timer = new Timer(300, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 加载数据
                //loadData();
            	String SQL = """
                		SELECT id,command_str,remark,update_DT,creat_DT AS DT FROM history
                		ORDER BY DT DESC
            		""";
                	try (Connection conn = DriverManager.getConnection(dbPath);
                         Statement stmt = conn.createStatement();
                         ResultSet rs = stmt.executeQuery(SQL)) {
                        while (rs.next()) {
                            Object[] rowData = {
                        		rs.getInt("id")
                        		, rs.getString("command_str")
                        		, rs.getString("remark")
                        		, rs.getString("DT")
                        	};
                            tableModel.addRow(rowData);
                        }

                    } catch (SQLException eDB) {
                    	eDB.printStackTrace();
                    }
                loadingLabel.setVisible(false);
            }
        });
        timer.setRepeats(false); // 只执行一次
        timer.start();
    }

    private void DB_Update(String[] args) {
        try (Connection connection = DriverManager.getConnection(dbPath)) {
            // 使用 prepareStatement 方法创建 PreparedStatement 对象，并设置参数化的 SQL 查询语句
            String sqlUpdate = "UPDATE history SET remark = ? WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);

            // 设置参数的值，注意参数索引从 1 开始
            preparedStatement.setString(1, args[1]);
            preparedStatement.setString(2, args[0]);

            // 执行更新操作
            int rowsUpdated = preparedStatement.executeUpdate();

            // 根据更新操作影响的行数判断更新是否成功
            if (rowsUpdated > 0) {
                System.out.println("用户记录更新成功！");
            } else {
                System.out.println("用户记录更新失败！");
            }
        } catch (SQLException e) {
            // 处理异常
            e.printStackTrace();
        }
    }
    
    private void DB_Delete (String[] args) {
        try (Connection connection = DriverManager.getConnection(dbPath)) {
            // 使用 prepareStatement 方法创建 PreparedStatement 对象，并设置参数化的 SQL 查询语句
            String sqlUpdate = "DELETE FROM history WHERE id IN ("+args[0]+")";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);


            // 执行更新操作
            int rowsUpdated = preparedStatement.executeUpdate();

            // 根据更新操作影响的行数判断更新是否成功
            if (rowsUpdated > 0) {
                System.out.println("用户记录刪除成功！");
            } else {
                System.out.println("用户记录刪除失败！");
            }
        } catch (SQLException e) {
            // 处理异常
            e.printStackTrace();
        }
    }

 // 在类中定义 DecimalFormat 对象
    private DecimalFormat decimalFormat = new DecimalFormat("#.##");
    
    private class ButtonClickListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();
            switch (command) {
                case "=":
                    // 计算结果
                    String expression = display.getText();
                    try {
                        double result = evaluateExpression(expression);
                        
                        // 格式化结果，保留两位小数
                        String formattedResult = decimalFormat.format(result);
                        
                        //String resultText = result % 1 == 0 ? String.valueOf((int) result) : String.valueOf(result);

                        display.setText(formattedResult);
                        
                        String[] command_str = {expression + "=" + formattedResult, "value2", "value3"};
                        DB_Create(command_str);
                        DB_Retrieve();
                        isPreviousButtonEqual=true;
                        historyTextArea.setText(expression + "=" + formattedResult + "\n" + historyTextArea.getText() );
                    } catch (Exception ex) {
                        display.setText("Error");
                    }
                    break;
                case "←":
                    // 删除最后一个字符
                    String currentText = display.getText();
                    if (currentText.length() > 0) {
                        display.setText(currentText.substring(0, currentText.length() - 1));
                    }
                    break;
                case "C":
                    // 清空显示
                    display.setText("");
                    break;
                case "":
                    Object source = e.getSource();
                    if (source instanceof JButton) {
                        JButton button = (JButton) source;
                        String toolTipText = button.getToolTipText();
                        // 现在你可以使用 toolTipText 进行你的逻辑处理
                        System.out.println("ToolTipText: " + toolTipText);
                        if (toolTipText.equals("history")) {
                        	historyTextArea.setVisible(!historyTextArea.isVisible());
                        	DB_Retrieve();
                            // 重新绘制界面
                            revalidate();
                        }
                    }
                    break;
                default:
                    // 更新显示

                	if (isPreviousButtonEqual) {
                		display.setText("");
                		isPreviousButtonEqual=false;
                	}
//                	String xx=display.getText();
//                	if (xx=="") {
//                		if (command=="+" || command=="-" || command=="×" || command=="÷") {
//                			command="";
//                		}
//                	}
                	display.setText(display.getText() + command);
                    break;
            }
        }
    }

    // 计算表达式结果的方法
    private double evaluateExpression(String expression) {
        // 移除所有空格
        expression = expression.replaceAll("\\s+", "");

        // 分割表达式
        String[] tokens = expression.split("(?=[-+×÷])|(?<=[-+×÷])");

        // 初始化结果为第一个操作数
        double result = Double.parseDouble(tokens[0]);

        // 遍历剩余的操作符和操作数
        for (int i = 1; i < tokens.length; i += 2) {
            String operator = tokens[i];
            double operand = Double.parseDouble(tokens[i + 1]);

            // 根据操作符执行相应的运算
            switch (operator) {
                case "+":
                    result += operand;
                    break;
                case "-":
                    result -= operand;
                    break;
                case "×":
                    result *= operand;
                    break;
                case "÷":
                    if (operand != 0) {
                        result /= operand;
                    } else {
                        throw new ArithmeticException("Division by zero");
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Invalid operator: " + operator);
            }
        }

        return result;
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        // 获取窗口的宽度和高度
        int width = getWidth();
        int height = getHeight();

        // 根据窗口的宽度和高度动态调整按钮的字体大小
        int fontSize = Math.min(width, height) / 10;
        Font buttonFont = new Font("Arial", Font.PLAIN, fontSize);
        for (JButton button : buttons) {
            button.setFont(buttonFont);
        }
        Font font = display.getFont().deriveFont((float) fontSize);
        display.setFont(font);
    }
    
    public void conn() {
    	try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//取得相對路徑
        String currentDir = null;
        try {
        	
            currentDir = new File(CalculatorPanel_CRUD.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        // 构建数据库文件路径，放置在当前 JAR 文件所在目录下
        dbPath = "jdbc:sqlite:" + currentDir + File.separator + "database.db";


         try (Connection connection = DriverManager.getConnection(dbPath)) {
             if (connection != null) {
                 System.out.println("Connected to the database.");

                 // 创建新的表格
                 Statement statement = connection.createStatement();

                 String createTableSQL = """
					CREATE TABLE IF NOT EXISTS history (
					    id INTEGER PRIMARY KEY,
					    command_str TEXT,
					    remark TEXT,
					    update_DT DATETIME ,
					    creat_DT DATETIME DEFAULT CURRENT_TIMESTAMP
					);
                		    """;
                 statement.execute(createTableSQL);
                 createTableSQL = """
					CREATE TRIGGER IF NOT EXISTS history_update_date_trigger
					AFTER UPDATE ON history
					FOR EACH ROW
					BEGIN
					    UPDATE history
					    SET update_DT = CURRENT_TIMESTAMP
					    WHERE id = OLD.id;
					END;
                		    """;
                 statement.execute(createTableSQL);
                 System.out.println("Table created successfully.");
             }
         } catch (SQLException e) {
             System.out.println("Error connecting to the database: " + e.getMessage());
         }
    }
    
    public void createAndShowGUI(CalculatorPanel_CRUD calculator) {
        frame = new JFrame("Calculator v2.0");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(calculator);
        frame.setSize(850, 450); 
        //frame.pack();
        frame.setVisible(true);
        isPreviousButtonEqual=false;
    }

    public static void main(String[] args) {
    	CalculatorPanel_CRUD calculator = new CalculatorPanel_CRUD();
    	calculator.conn(); // 使用类的实例调用非静态方法	
    	calculator.createAndShowGUI(calculator);
    }

}
