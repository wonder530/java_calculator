import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.imageio.ImageIO;
import java.io.*;
import java.text.DecimalFormat;

public class CalculatorPanel extends JPanel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField display;
    private JButton[] buttons;
    private JTextArea historyTextArea;

    public CalculatorPanel() {
        setLayout(new BorderLayout());

        // 创建显示结果的文本框
        display = new JTextField();
        display.setEditable(true);
        add(display, BorderLayout.NORTH);

        // 创建按钮面板，并设置布局为网格布局
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(5, 4));

        // 按钮的标签
        String[] buttonLabels = {
        		"history", "","C","←",
        		"7", "8", "9", "÷",
                "4", "5", "6", "×",
                "1", "2", "3", "-",
                ".", "0", "=", "+" 
        };

        // 初始化按钮数组
        buttons = new JButton[buttonLabels.length];
        
        // 创建按钮并添加到面板上
        for (int i = 0; i < buttonLabels.length; i++) {
            buttons[i] = new JButton(buttonLabels[i]);
	        if (buttonLabels[i].equals("history")) {
	            try {
	                // 读取图片文件并创建图标
	                ImageIcon icon = new ImageIcon(ImageIO.read(new File("res/history.png")));
	                buttons[i].setIcon(icon);
	                buttons[i].setText("");
	                buttons[i].setToolTipText("history");
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }else {
	            // 设置按钮字体大小
	            buttons[i].setFont(new Font("Arial", Font.PLAIN, (buttonLabels[i].equals("←"))?10:40));   	
	        }

            

            buttonPanel.add(buttons[i]);
        }

        // 添加按钮面板到主面板的中间
        add(buttonPanel, BorderLayout.CENTER);

        // 添加按钮事件监听器
        for (JButton button : buttons) {
            button.addActionListener(new ButtonClickListener());
        }
        //準備右側的歷史表單
        //JPanel historyPanel = new JPanel();
        // 创建历史文本区域并设置固定宽度
        historyTextArea = new JTextArea(10,5); // 设置行数和列数
        historyTextArea.setEditable(true); // 禁止编辑
        JScrollPane scrollPane = new JScrollPane(historyTextArea); // 添加滚动条
        
        
        add(scrollPane, BorderLayout.EAST);
    }
 // 在类中定义 DecimalFormat 对象
    private DecimalFormat decimalFormat = new DecimalFormat("#.##");
    
    private class ButtonClickListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();
            switch (command) {
                case "=":
                    // 计算结果
                    String expression = display.getText();
                    try {
                        double result = evaluateExpression(expression);
                        
                        // 格式化结果，保留两位小数
                        String formattedResult = decimalFormat.format(result);
                        
                        //String resultText = result % 1 == 0 ? String.valueOf((int) result) : String.valueOf(result);

                        display.setText(formattedResult);
                        
                        historyTextArea.setText(expression + "=" + formattedResult + "\n" + historyTextArea.getText() );
                    } catch (Exception ex) {
                        display.setText("Error");
                    }
                    break;
                case "←":
                    // 删除最后一个字符
                    String currentText = display.getText();
                    if (currentText.length() > 0) {
                        display.setText(currentText.substring(0, currentText.length() - 1));
                    }
                    break;
                case "C":
                    // 清空显示
                    display.setText("");
                    break;
                case "":
                    Object source = e.getSource();
                    if (source instanceof JButton) {
                        JButton button = (JButton) source;
                        String toolTipText = button.getToolTipText();
                        // 现在你可以使用 toolTipText 进行你的逻辑处理
                        System.out.println("ToolTipText: " + toolTipText);
                        if (toolTipText.equals("history")) {
                        	historyTextArea.setVisible(!historyTextArea.isVisible());
                            // 重新绘制界面
                            revalidate();
                        }
                    }
                    break;
                default:
                    // 更新显示
                    display.setText(display.getText() + command);
                    break;
            }
        }
    }

    // 计算表达式结果的方法
    private double evaluateExpression(String expression) {
        // 移除所有空格
        expression = expression.replaceAll("\\s+", "");

        // 分割表达式
        String[] tokens = expression.split("(?=[-+×÷])|(?<=[-+×÷])");

        // 初始化结果为第一个操作数
        double result = Double.parseDouble(tokens[0]);

        // 遍历剩余的操作符和操作数
        for (int i = 1; i < tokens.length; i += 2) {
            String operator = tokens[i];
            double operand = Double.parseDouble(tokens[i + 1]);

            // 根据操作符执行相应的运算
            switch (operator) {
                case "+":
                    result += operand;
                    break;
                case "-":
                    result -= operand;
                    break;
                case "×":
                    result *= operand;
                    break;
                case "÷":
                    if (operand != 0) {
                        result /= operand;
                    } else {
                        throw new ArithmeticException("Division by zero");
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Invalid operator: " + operator);
            }
        }

        return result;
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        // 获取窗口的宽度和高度
        int width = getWidth();
        int height = getHeight();

        // 根据窗口的宽度和高度动态调整按钮的字体大小
        int fontSize = Math.min(width, height) / 10;
        Font buttonFont = new Font("Arial", Font.PLAIN, fontSize);
        for (JButton button : buttons) {
            button.setFont(buttonFont);
        }
        Font font = display.getFont().deriveFont((float) fontSize);
        display.setFont(font);
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("Calculator v1.0");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new CalculatorPanel());
        frame.pack();
        frame.setVisible(true);
    }
}
