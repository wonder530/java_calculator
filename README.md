## Introduction
每當工作條件是不熟悉的語言時，總是覺得以過往的程式開發經驗可以很快上手。
但光說不練，連自己都很難說服，更何況是對別人介紹呢。於是就嘗試用Eclipse來開發JAVA應用程式看看，搭配GPT去學習新程式開發語言。

## Screenshots

![Screenshot](res/screenshots/v1.0.gif)

![Screenshot](res/screenshots/v2.0.gif)

## Features and Versions
  - v1.0 基礎計算機(CalculatorPanel.java)
    - 具備可放大縮小改字體功能
    - 按鈕可以載入icon
    - 簡易歷史紀錄(文字區域顯示,未儲存)
    - 搭配Gitlab進行版本管控
    - 搭配markdown語法產生文件說明
    - 將計算過程儲存到資料庫
  - v2.0 基礎計算機(CalculatorPanel_CRUD.java)
    - 使用SQLlite當作資料庫進行CRUD
    - 版面布局調整
    - 使表格支援表頭點選排序
    - 增加讀取loading gif顯示
    - 表格右鍵菜單與刪除確認
  - (預定)v3.0 基礎計算機-動態載入
    - 調整UTC時間為+8台灣時間
    - 將歷史資料面板獨立成java檔案並支援動態載入
    
## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
